export default{
    data: function(){
        return{
            errors: [],
            email: null,
            password: null,
            confirm_password: null  
        }
      },
      methods:{
        checkForm: function (e) {
           
          if (this.email && this.password) {
            return true;
          }
          
          this.errors = [];
          
          if (!this.email) {
            this.errors.push('email required.');
          }else if (!this.validEmail(this.email)) {
            this.errors.push('Valid email required.');        
          }
          if (!this.password) {
            this.errors.push('password required.');
          }
          if (!this.errors.length) {
            return true;
          }
          e.preventDefault();
        },
        checkforgetpass: function (e) {
           
            if (this.email && this.password && this.confirm_password) {
                alert('enterrrrrrrr');
               return true;
            }else if (this.password != this.confirm_password) {
                alert('enter');
                this.errors.push('password does not match');
            }

            this.errors = [];
            
            if (!this.email) {
              this.errors.push('email required.');
            }else if (!this.validEmail(this.email)) {
              this.errors.push('Valid email required.');        
            }
            if (!this.password) {
              this.errors.push('password required.');
            }
            if (!this.confirm_password) {
              this.errors.push('confirm password required.');
            }
            if (!this.errors.length) {
                return true;
            }
                
            e.preventDefault();
          },
        validEmail: function (email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
          }
      }
}
