'use strict'

import Vue from 'vue'
import Router from 'vue-router'
import MyComponent from '@/components/MyComponent'
import HomeLayout from '@/components/HomeLayout'
import loginLayout from '@/components/loginLayout'
import login from '@/components/login'
import forgetPassword from '@/components/forgetPassword'
import error from '@/components/error'
import homePage from '@/components/homePage'
import orderLayout from '@/components/orderLayout'
import orderSuccess from '@/components/orderSuccess'
Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: MyComponent,
      children: [
        {
          path: '/',
          component: loginLayout,
          children: [
            {
              path: '/',
              component: login
            },
            {
              path: 'forgetPassword',
              component: forgetPassword
            },
            {
            path: '/error',
            component: error
            },
            {
              path: '/orderSuccess',
              component: orderSuccess
            }
          ] 
        },
      ]
    },
   {
     path: '/HomeLayout',
     component: HomeLayout,
     children: [
      {
        path: '/',
        component: homePage
      },
      {
        path: '/orderLayout',
        component: orderLayout
      }
    ] 
   },
  ]
})
